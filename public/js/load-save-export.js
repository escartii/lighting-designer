
/**
 * Functions to save and load app data. Functions to export to different file formats.
 */

function downloadSVG() {
    canvas.discardActiveObject().renderAll();

    let svg = canvas.toSVG();
    let downloadLink = document.createElement('a');
    downloadLink.setAttribute('href', 'data:image/svg+xml;base64,' + btoa(svg));
    downloadLink.setAttribute('download', 'my-project.svg');
    downloadLink.click();
}

function downloadPNG() {
    canvas.discardActiveObject().renderAll();

    let canvasHTML = document.getElementById('c');
    canvasHTML.toBlob(function (blob) {
        let url = URL.createObjectURL(blob);
        let downloadLink = document.createElement('a');
        downloadLink.setAttribute('href', url);
        downloadLink.setAttribute('download', 'my-project.png');
        downloadLink.click();
    });

}

/* Use jsPDF to generate a PDF from the HTML canvas */
const { jsPDF } = window.jspdf;
function downloadPDF() {
    // We may want to check out https://stackoverflow.com/questions/16858954/how-to-properly-use-jspdf-library
    // Also check out http://raw.githack.com/MrRio/jsPDF/master/docs/module-svg.html
    canvas.discardActiveObject().renderAll();

    let canvasHTML = document.getElementById('c');
    let imgData = canvasHTML.toDataURL("image/png");
    let pdf = new jsPDF({
        orientation: 'p',
        unit: 'mm',
        format: 'a4',
        putOnlyUsedFonts: true,
        floatPrecision: 16 // or "smart", default is 16
    });

    // Add project title
    pdf.setFont("helvetica", "normal", "bold");
    pdf.setFontSize(18);
    pdf.text('My Project', 25, 25);

    // Add canvas as an image
    let aspectRatio = canvasHTML.width / canvasHTML.height;
    let imgW = 210 - 25 * 2;
    let imgH = imgW / aspectRatio;
    pdf.addImage(imgData, 'PNG', 25, 40, imgW, imgH);

    // Add credits (URL should render as a link)
    pdf.setFont("helvetica", "normal", "normal");
    pdf.setFontSize(10);
    pdf.text('Diagram generated with Lighting Designer (https://guyikcgg.gitlab.io/lighting-designer/)', 25, 297 - 25);

    // Download PDF
    pdf.save("my-project.pdf");
}

// Save project to a JSON file (currently, just the canvas with FabricJS)
function saveJSON() {
    canvas.discardActiveObject().renderAll();
    
    // TODO We may want to use showSaveFilePicker() in the future
    let json = canvas.toJSON();
    let file = new Blob([JSON.stringify(json)], { type: 'text/plain' });
    let downloadLink = document.createElement('a');
    downloadLink.href = URL.createObjectURL(file);
    downloadLink.setAttribute('download', 'my-project.json');
    downloadLink.click();
}

// Load project from a JSON file (currently, just the canvas with FabricJS)
function loadJSON() {
    var input = document.createElement('input');
    input.type = 'file';
    input.accept = 'application/JSON';

    input.onchange = function (e) {
        // getting a hold of the file reference
        var file = e.target.files[0];

        // setting up the reader
        var reader = new FileReader();
        reader.readAsText(file); // this is reading as text

        // here we tell the reader what to do when it's done reading...
        reader.onload = function (readerEvent) {
            var content = readerEvent.target.result; // this is the content!
            canvas.loadFromJSON(content, canvas.renderAll.bind(canvas));
        }
    }

    input.click();
}

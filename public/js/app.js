$(document).foundation()

// Set up the canvas
var canvas = this.__canvas = new fabric.Canvas('c');
resizeCanvas();
preventObjectsEscapingTheCanvas();
setupCustomControls();

// Customize controls
//fabric.Object.prototype.transparentCorners = false;
//fabric.Object.prototype.cornerColor = 'blue';
fabric.Object.prototype.cornerStyle = 'circle';

// Canvas objects
function addObjectFromAsset(path, scale = 1, left = 50, top = 50) {
    fabric.loadSVGFromURL(path, function (objects, options) {
        var shape = fabric.util.groupSVGElements(objects, options);
        shape.setControlsVisibility({
            mt: false, // middle top 
            mb: false, // midle bottom
            ml: false, // middle left
            mr: false, // middle right
            tl: true, //top left
            tr: true, //top right
            bl: true, //bottom left
            br: true //bottom right
        });

        canvas.add(shape.scale(scale));
        canvas.setActiveObject(shape);

        shape.set({ left: left, top: top }).setCoords();
        canvas.renderAll();

        canvas.forEachObject(function (obj) {
            var setCoords = obj.setCoords.bind(obj);
            obj.on({
                moving: setCoords,
                scaling: setCoords,
                rotating: setCoords
            });
        })
    });

}


// Add shapes
function shape(type, width = 100, height = 100, left = 50, top = 50) {
    shapeOptions = {
        left: left,
        top: top,
        fill: 'lightgray',
        objectCaching: false,
        stroke: 'darkgray',
        strokeWidth: 2,
        strokeUniform: true,
    };

    switch (type) {
        case "rect":
            shapeOptions.width = width;
            shapeOptions.height = height;
            var shape = new fabric.Rect(shapeOptions);
            break;

        case "circle":
            shapeOptions.radius = Math.min(width, height)/2;
            var shape = new fabric.Circle(shapeOptions);
            break;

        case "triangle":
            shapeOptions.width = width;
            shapeOptions.height = Math.sqrt(3)*width/2;
            var shape = new fabric.Triangle(shapeOptions);
            break;

        default:
            console.error("unknown shape type");
            break;
    }

    shape.setControlsVisibility({
        mt: true, // middle top 
        mb: true, // midle bottom
        ml: true, // middle left
        mr: true, // middle right
        tl: true, //top left
        tr: true, //top right
        bl: true, //bottom left
        br: true //bottom right
    });

    canvas.add(shape);
    canvas.setActiveObject(shape);
}

// Dark mode toggle
function darkMode() {
    var body = document.body;
    body.classList.toggle('dark-mode');
}

/*
objectsMenu.forEach(function(e) {
    var li = document.createElement('li');
    var a = document.createElement('a');
    a.setAttribute('href', '#');
    a.innerHTML = e.name;
    var ul = document.createElement('ul');
    ul.setAttribute('class', 'menu vertical nested');
    var div = document.createElement('div');
    div.setAttribute('class', 'hollow button-group button-cell');
    e.buttons.forEach(function (b) {
        let img = document.createElement('img');
        img.setAttribute('src', 'assets/' + b.icon);
        img.setAttribute('alt', b.name);
        let button = document.createElement('button');
        button.setAttribute('title', b.name);
        button.setAttribute('class', 'button');
        button.dataset.tooltip = true;
        button.setAttribute('onclick', "addObjectFromAsset('assets/" + b.icon + "', " + b.defaultScale + ")");
        button.appendChild(img);
        div.appendChild(button);
        let li2 = document.createElement('li');
        li2.appendChild(div);
        ul.appendChild(li2);
    });
    li.appendChild(a);
    li.appendChild(ul);
    document.getElementById('objects').appendChild(li);
});
*/

// Add default objects to canvas (when the document has fully loaded)
window.addEventListener("load", function () {
    addObjectFromAsset('assets/subject-1.svg', 0.3);
    addObjectFromAsset('assets/dslr.svg', 0.7, 110, 300);
});


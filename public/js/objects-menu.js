
objectsMenu = [
    {
        name: "Subjects",
        buttons: [
            {
                name: "Generic subject",
                icon: "subject-1.svg",
                defaultScale: 0.3
            }
        ]
    },
    {
        name: "Light sources",
        buttons: [
            {
                name: "Flash with parabola",
                icon: "flash-parabola.svg",
                defaultScale: 0.7
            },
            {
                name: "Flash with softbox",
                icon: "flash-softbox.svg",
                defaultScale: 0.7
            },
            {
                name: "Fresnel",
                icon: "fresnel.svg",
                defaultScale: 0.7
            }
        ]
    },
    {
        name: "Accessories",
        buttons: [
            {
                name: "DSLR Camera",
                icon: "dslr.svg",
                defaultScale: 0.7
            },
            {
                name: "Light box",
                icon: "light-box.svg",
                defaultScale: 0.5
            },
            {
                name: "White panel",
                icon: "white-panel.svg",
                defaultScale: 4.0
            },
            {
                name: "Black panel",
                icon: "black-panel.svg",
                defaultScale: 4.0
            },
            {
                name: "Diffusor panel",
                icon: "diffusor-panel.svg",
                defaultScale: 4.0
            },
            {
                name: "Softbox",
                icon: "softbox.svg",
                defaultScale: 0.7
            },
            {
                name: "Light meter",
                icon: "light-meter.svg"
            },
            {
                name: "White reflector",
                icon: "reflector-white.svg",
                defaultScale: 0.7
            },
            {
                name: "Black reflector",
                icon: "reflector-black.svg",
                defaultScale: 0.7
            },
            {
                name: "Silver reflector",
                icon: "reflector-silver.svg",
                defaultScale: 0.7
            },
            {
                name: "Golden reflector",
                icon: "reflector-golden.svg",
                defaultScale: 0.7
            },
            {
                name: "White umbrella",
                icon: "umbrella-white.svg",
                defaultScale: 0.6
            },
            {
                name: "Black umbrella",
                icon: "umbrella-black.svg",
                defaultScale: 0.6
            },
        ]
    },
]

objectsMenu.forEach(function(e) {
    $('#objects').append(
        '<li class="accordion-item" data-accordion-item>'
         + '<a href="#" class="accordion-title">' + e.name + '</a>'
         + '<div class="accordion-content" data-tab-content>'
           + '<div class="hollow button-group button-cell"></div>'
         + '</div>'
      + '</li>'
    );
    e.buttons.forEach(function (b) {
        $('#objects .button-group:last').append(
            '<button class="button" data-tooltip title="' + b.name + '"'
             + 'onclick="addObjectFromAsset(\'assets/' + b.icon + '\', ' + b.defaultScale + ')">'
                + '<img src="assets/' + b.icon + '" alt="' + b.name + '"/>'
          + '</button>'
        );
    });
});
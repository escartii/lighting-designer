function resizeCanvas() {
    canvas.setDimensions({
        width: $('.canvas-container').width(),
        height: $(window).height() - $('#header').height() - 10
    });
    canvas.renderAll();
}

window.addEventListener('resize', resizeCanvas, false);
